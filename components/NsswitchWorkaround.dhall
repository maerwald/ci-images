let
  CF = ../deps/Containerfile.dhall

--| systemd isn't running so remove it from nsswitch.conf
-- Failing to do this will result in testsuite failures due to
-- non-functional user lookup (#15230).
--
-- This is necessary on Redhat-based distributions running under Docker as they
-- enable the systemd nsswitch provider for passwd and group resolution.
let nsswitchWorkaround: CF.Type =
  CF.run "Disable systemd nsswitch provider" [ "sed -i -e 's/systemd//g' /etc/nsswitch.conf" ]
in nsswitchWorkaround
