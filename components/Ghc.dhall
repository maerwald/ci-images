let
  Prelude = ../deps/Prelude.dhall
let
  CF = ../deps/Containerfile.dhall

let
  BindistSpec: Type =
    { version : Text
    , triple : Text
    }

let
  Options =
    { Type = 
      { bindist : BindistSpec
      , destDir : Text
      , configureOpts : List Text
      }
    , default = 
      { configureOpts = [] : List Text
      }
    }

let
  install: Options.Type -> CF.Type =
    \(opts: Options.Type) ->
        let
          version = opts.bindist.version
        let
          bindistUrl: Text = "https://downloads.haskell.org/~ghc/${version}/ghc-${version}-${opts.bindist.triple}.tar.xz"
        in
            CF.run "fetch GHC" [ "curl -L ${bindistUrl} | tar -Jx -C /tmp" ]
          # CF.run "configure bindist"
          [ "cd /tmp/ghc-${version}*"
          , "./configure ${Prelude.Text.concatSep " " opts.configureOpts} --prefix=${opts.destDir}"
          , "make install"
          ]
          # CF.run "remove temporary directory" [ "rm -Rf /tmp/ghc-${version}-*" ]
          # CF.run "test GHC" [ "${opts.destDir}/bin/ghc --version" ]

in
{ Options = Options
, BindistSpec = BindistSpec
, install = install
}