-- Defines the child GitLab CI pipeline which builds our Docker images.

let
  Prelude = ./deps/Prelude.dhall
let
  Map = Prelude.Map
let
  CF = ./deps/Containerfile.dhall
let
  Image = ./Image.dhall
let
  GitLab = ./deps/GitLabCi.dhall

let
  images: List Image.Type = ./images.dhall

let
  mkJobEntry: Text -> GitLab.Job.Type -> Prelude.Map.Entry Text GitLab.Job.Type =
    Prelude.Map.keyValue GitLab.Job.Type

let
  toJob: Image.Type -> Prelude.Map.Entry Text GitLab.Job.Type =
    \(image: Image.Type) ->
    let 
      docker_base_url: Text = "registry.gitlab.haskell.org/$CI_PROJECT_PATH"
    let
      imageName: Text = "${docker_base_url}/${image.name}"

    let job: GitLab.Job.Type =
      GitLab.Job::
      { image = Some "docker:19.03"
      , stage = Some image.jobStage
      , tags = Some (image.runnerTags # [ "docker" ])
      , variables = toMap
        { DOCKER_DRIVER = "overlay2"
        , DOCKER_TLS_CERTDIR = "/certs"
        }
      , needs = image.needs # [ "generate-dockerfiles" ]
      , dependencies = [ "generate-dockerfiles" ]
      , services = Some [ "docker:19.03-dind" ] 
      , before_script = Some [ "docker info" ]
      , script =
        [ "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
        , "docker pull ${imageName}:latest || true"
        , "docker build --cache-from ${imageName}:latest --tag ${imageName}:$CI_COMMIT_SHA --tag ${imageName}:latest dockerfiles/${image.name}"
        , ''
          if [ -z "$CI_MERGE_REQUEST_ID" ]; then
              docker push ${imageName}:$CI_COMMIT_SHA
              docker push ${imageName}:latest
          fi
          ''
        ]
      }

    in mkJobEntry image.name job

let
  dockerfilesJob =
    let
      dhallUrl = "https://github.com/dhall-lang/dhall-haskell/releases/download/1.34.0/dhall-1.34.0-x86_64-linux.tar.bz2"
    in GitLab.Job::
      { stage = Some "prepare"
      , image = Some "debian:buster"
      , tags = Some [ "x86_64-linux" ]
      , script = [
        , "mkdir -p dockerfiles"
        , "apt-get update"
        , "apt-get install -y curl tar bzip2"
        , "curl -L ${dhallUrl} | tar -jx"
        , "./bin/dhall to-directory-tree --file=dockerfiles.dhall --output=dockerfiles"
        ]
      , artifacts =
        Some (GitLab.ArtifactsSpec:: { paths = [ "dockerfiles" ] })
      }

let
  lintJob =
    GitLab.Job::
    { stage = Some "lint"
    , image = Some "hadolint/hadolint:latest-debian"
    , tags = Some [ "x86_64-linux" ]
    , script = 
      [ "find dockerfiles -name Dockerfile -print0 | xargs -0 -n1 hadolint" ]
    }

let
  jobs: Prelude.Map.Type Text GitLab.Job.Type =
    Prelude.List.map Image.Type (Prelude.Map.Entry Text GitLab.Job.Type) toJob images

let top: GitLab.Top.Type =
  GitLab.Top::
  -- N.B. Jobs sadly can only depend upon jobs in previous stages. Hence the
  -- `build-derived` stage, which exists only for the `linter` job.
  { stages = Some [ "prepare", "lint", "build", "build-derived" ]
  , jobs = jobs # toMap 
    { generate-dockerfiles = dockerfilesJob
    -- , lint = lintJob
    }
  }

in
Prelude.JSON.renderYAML (GitLab.Top.toJSON top)